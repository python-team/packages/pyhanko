# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/pyhanko/issues
# Bug-Submit: https://github.com/<user>/pyhanko/issues/new
# Changelog: https://github.com/<user>/pyhanko/blob/master/CHANGES
# Documentation: https://github.com/<user>/pyhanko/wiki
# Repository-Browse: https://github.com/<user>/pyhanko
# Repository: https://github.com/<user>/pyhanko.git
