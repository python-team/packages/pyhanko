% pyhanko(1) | User Commands
%
% "February 19 2024"


# NAME

pyhanko - program to handle digitally signing and stamping PDF files

# SYNOPSIS

**pyhanko** [OPTIONS] COMMAND [ARGS]...

# OPTIONS

**\-\-version**
:    Show the version and exit.

**\-\-config FILENAME**
:    YAML file to load configuration from[default:pyhanko.yml]

**\-\-verbose**
:    Run in verbose mode

**\-\-no-plugins**
:    Disable non-builtin plugin loading

**\-\-help**
:    Show this message and exit.

# COMMANDS
**decrypt**
:    decrypt PDF files (any standard PDF encryption scheme)

**encrypt**
:    encrypt PDF files (AES-256 only)

**sign**
:    sign PDFs and other files

**stamp**
:    stamp PDF files


# AUTHOR

Georges Khaznadar <georgesk@debian.org>
:   Wrote this manpage for the Debian system.

# COPYRIGHT

Copyright © 2024 Georges Khaznadar

This manual page was written for the Debian system (and may be used by
others).

Permission is hereby granted, free of charge, to any person obtaining a
copy of this software and associated documentation files (the "Software"),
to deal in the Software without restriction, including without limitation
the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included
in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
